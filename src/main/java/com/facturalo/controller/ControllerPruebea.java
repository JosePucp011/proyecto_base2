package com.facturalo.controller;

import com.facturalo.model.entity.Example;
import com.facturalo.service.dao.ExampleDAO;
import com.facturalo.service.interfac.IExample;
import com.facturalo.service.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ControllerPruebea {

    @Autowired
    private ExampleService exampleService;
    //private IExample iExample;

    @GetMapping("/saludo")
    public String saludo() {
        return "Este es un saludo de prueba";
    }


    //@GetMapping("/example")
    //@GetMapping(value = "/example", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value="/example", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Example> example() {
        return exampleService.findAllExample();
    }
}
