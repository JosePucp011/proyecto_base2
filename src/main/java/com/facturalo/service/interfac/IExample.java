package com.facturalo.service.interfac;

import com.facturalo.model.entity.Example;

import java.util.List;


public interface IExample {

    public List<Example> findAllExample();
}
