package com.facturalo.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "example")
public class Example {

    private @Id
    @GeneratedValue
    Integer id;

    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private String lastName;
    @Column(name = "email")
    private String email;

}
